/*
* Copyright (c) 2017, Francis Laniel, francis.laniel@lip6.fr
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "CacheLru.h"
#include <iostream>

using std::cerr;
using std::endl;
using std::map;
using std::cout;
using std::hex;
using std::dec;
using Caches::Page;
using Caches::CacheLru;

/**
* CacheLru constructor.
*
* @param bytes The syze of the LRU in bytes. If it is not given the size will be
* Page::size.
*/
CacheLru::CacheLru(unsigned long long bytes) :
	mapped_nr_(0){
	size_t i;
	size_t page_size;
	unsigned long long page_nr;

	page_size = Page::getPageSize();

	page_nr = bytes / page_size;

	if(not page_nr){
		cerr << RED << "Lru can not be empty : page_nr_ = '" << page_nr << "'." <<
		NO_COLOR << endl;

		exit(EXIT_FAILURE);
	}

	page_nr_ = page_nr;

	// Initialization of all DRAMPage.
	for(i = 0; i < page_nr_; ++i)
		pages_.push_back(new DRAMPage(i * page_size));
}

/**
* CacheLru destructor.
* It just deletes the DRAMPage allocated in constructor.
*/
CacheLru::~CacheLru(){
	unsigned long long i;

	for(i = 0; i < page_nr_; ++i)
		delete pages_[i];
}

/**
* Test if a NVRAMPage is contained in LRU.
*
* @param nvPage The NVRAMPage to search.
* @return If nvPage produced an HIT it returns the DRAMPage to which nvPage is
* mapped, it returns NULL otherwise.
*/
Page *CacheLru::testHit(NVRAMPage *nvPage){
	map<NVRAMPage *, DRAMPage *>::const_iterator it;

	for(it = directory_.begin(); it != directory_.end(); ++it)
		if(it->first->getBeginAddr() == nvPage->getBeginAddr())
			return it->second;
	return NULL;
}

/**
* This function handle a MISS in DRAM.
*
* It will search a free DRAMPage (or free one by ousting) and map the NVRAMPage
* to it.
*
* @param nvPage the NVRAMPage which will be mapped.
* @return the Page to which the NVRAMPage was mapped.
*/
Page *CacheLru::doMiss(NVRAMPage *nvPage){
	unsigned long long i;

	DRAMPage *toMap;

	cout << "Transaction to read from NVRAM and write to DRAM are not yet"
	" implemented." << endl;

	toMap = NULL;

	if(this->full()){
		toMap = dynamic_cast<DRAMPage *>(oust());

		if(toMap == NULL){
			cerr << RED << "dynamic_cast to DRAMPage* failed." <<
			NO_COLOR << endl;

			exit(EXIT_FAILURE);
		}
	}else{
		/*
		* The LRU is not full, so there is a DRAMPage which has not a NVRAM page
		* mapped.
		* We will search for it and break the research when found.
		*/
		for(i = 0; i < getPageNumber(); ++i)
			if(not pages_[i]->mapped()){
				toMap = pages_[i];

				break;
			}
	}

	if(toMap == NULL){
		cerr << RED << "The DRAMPage to which NVRAM Page will be mapped is NULL." <<
		NO_COLOR << endl;

		exit(EXIT_FAILURE);
	}

	mapped_nr_++; // Increment the number of NVRAM mapped pages.
	toMap->setNVRAMPage(nvPage); // Map the NVRAM page.

	/*
	* Add a new (key, value) or update an existing key to the map.
	* ATTENTION The insert_or_assign function is only available in c++ 17 so you
	* need to compile it with -std=c++1z on g++.
	*/
	directory_.insert_or_assign(nvPage, toMap);

	return toMap;
}

/**
* Oust a page from the LRU.
*
* This function searchs for the Least Recently Used page and oust it. If the
* page was dirty her data are written to NVRAM.
*
* @return The DRAMPage which was ousted. It is now ready to be linked to a new
* NVRAMPage.
*/
Page *CacheLru::oust(){
	unsigned long long i;

	clock_t lastAccess;
	clock_t minAccess;

	DRAMPage *ret;

	minAccess = time(0);
	ret = NULL;

	/*
	* Search the least recently used page.
	* We can search directly from the LRU and not from the directory_ because we
	* will oust page only if the LRU is full (see the doMiss function and the
	* 'if(this->full())' condition).
	*
	* TODO Speed up this search by using openmp or maintain a pointer on the
	* least recently used DRAMPage.
	*/
	for(i = 0; i < getPageNumber(); ++i){
		/*
		* We do not need to call if(pages_[i]->mapped()) because oust() is only
		* called in transaction() and the verification is done over there.
		*/
		lastAccess = pages_[i]->getLastAccess();

		if(lastAccess < minAccess){
			minAccess = lastAccess;

			ret = pages_[i];
		}
	}

	if(ret == NULL){
		cerr << RED << "The page which should be ousted is NULL." << NO_COLOR <<
		endl;

		exit(EXIT_FAILURE);
	}

	if(ret->dirty())
		cout << "Page is dirty. It has to be written back to NVRAM."
		" Transaction to do that are not yet implemented." << endl;

	mapped_nr_--;

	return ret;
}

/**
* This function makes a complete transaction in the LRU.
*
* If the NVRAMPage given in parameter is not present it
* will be bring back from NVRAM and mapped to DRAMPage in LRU.
*
* TODO Add a transaction type (READ or WRITE) and handle it.
*
* @param nvPage The NVRAMPage to search in the LRU.
* @return true if the transaction made an HIT or false otherwise.
*/
bool CacheLru::transaction(NVRAMPage *nvPage){
	bool ret;

	Page *page;
	DRAMPage *dramPage;

	if(not nvPage){
		cerr << RED << "Argument is NULL." << NO_COLOR << endl;

		exit(EXIT_FAILURE);
	}

	ret = true;

	page = testHit(nvPage);

	if(not page){
		cout << "MISS on page whom beginning address is '0x" << hex <<
		nvPage->getBeginAddr() << dec << "'." << endl;

		ret = false;

		page = doMiss(nvPage);
	}else{
		cout << "HIT on page whom beginning address is '0x" << hex <<
		nvPage->getBeginAddr() << dec << "'." << endl;
	}


	// We only dynamic_cast at the end of the if to avoid copying/pasting of code.
	dramPage = dynamic_cast<DRAMPage *>(page);

	if(dramPage == NULL){
		cerr << RED << "dynamic_cast to DRAMPage* failed." <<
		NO_COLOR << endl;

		exit(EXIT_FAILURE);
	}

	dramPage->refreshLastAccess();

	return ret;
}