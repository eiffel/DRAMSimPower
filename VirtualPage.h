/*
* Copyright (c) 2017, Francis Laniel, francis.laniel@lip6.fr
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef PAGEVIRTUAL_H
#define PAGEVIRTUAL_H

#include <cstdlib>
#include "Page.h"

namespace Caches{
	/**
	* This class will represent the virtual page since the input will be based
	* on the output of a modified valgrind which gives virtual address.
	*/
	class VirtualPage : public Page {
		public :
			VirtualPage(unsigned long long addr);
			~VirtualPage();

			/**
			* Getter for the PhysicalPage associated to this VirtualPage.
			*
			* @return The PhysicalPage to whom this VirtualPage is mapped.
			*/
			inline PhysicalPage *getPhysicalPage(){
				return mappedTo_;
			}

			/**
			* Setter for the PhysicalPage associated to this VirtualPage.
			*
			* @param page The PhysicalPage to whom this VirtualPage will be mapped.
			*/
			inline void setPhysicalPage(PhysicalPage *page){
				mappedTo_ = page;
			}

		private :
			PhysicalPage *mappedTo_; /**< If a VirtualPage is not mapped then it will
			produce a page fault. Once the page fault was handled it is then mapped
			to a PhysicalPage. */
	};
}
#endif