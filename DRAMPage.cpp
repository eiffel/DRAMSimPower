/*
* Copyright (c) 2017, Francis Laniel, francis.laniel@lip6.fr
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "DRAMPage.h"


using Caches::DRAMPage;

/**
* DRAMPage constructor.
*
* @param addr The beginning address of the page.
*/
DRAMPage::DRAMPage(unsigned long long addr) :
	Page(addr),
	lastAccess_(0),
	dirty_(false),
	pageMapped_(NULL){
}

/**
* DRAMPage destructor.
*
* It does nothing.
*/
DRAMPage::~DRAMPage(){
}

/**
* This function refreshes the last access date (i.e. it sets it to zero).
*/
void DRAMPage::refreshLastAccess(){
	/*
	* Instead of setting this data to zero we put the number of clock cycles since
	* the beginning of the programm execution. So we do not need to increment the
	* date all of mapped DRAMPage.
	* The research of the least recently used DRAMPage will consist in searching
	* the minimum value for lastAccess_.
	*/
	lastAccess_ = clock();
}