/*
* Copyright (c) 2017, Francis Laniel, francis.laniel@lip6.fr
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DRAMPAGE_H
#define DRAMPAGE_H

#include "Page.h"
#include <time.h>


namespace Caches{
	/**
	* Our DRAM will be used as a cache for the NVRAM.
	*
	* A page in DRAM will map a NVRAM page.
	*/
	class DRAMPage : public PhysicalPage {
		public :
			DRAMPage(unsigned long long addr);
			~DRAMPage();

			void refreshLastAccess();

			/**
			* Getter for NVRAM page's beginning address.
			*
			* @return The value of the nvAddr_ class member.
			*/
			inline unsigned long long getNVAddr(){
				if(not mapped())
					// TODO Replace this dirty throw with a true exception class.
					throw "There is no NVRAM page mapped to this DRAMPage";
				return pageMapped_->getBeginAddr();
			}

			/**
			* Getter for last access data of this DRAM page.
			*
			* @return The value of the lastAccess_ class member.
			*/
			inline time_t getLastAccess(){
				return lastAccess_;
			}

			/**
			* Setter for NVRAM page's.
			*
			* It also resets the last access date for this DRAM page and sets its
			* mapped_ variable as true since a NVRAM page is mapped into this DRAM
			* page.
			*
			* @param addr The address of the NVRAM page.
			*/
			inline void setNVRAMPage(NVRAMPage *page){
				pageMapped_ = page;
			}

			/**
			* Function which indicates if a NVRAM page is mapped to this DRAM page.
			*
			* @return true if the DRAMPage has a mapped page, false otherwise.
			*/
			inline bool mapped(){
				return pageMapped_ ? true : false;
			}

			/**
			* Indicates if the DRAM page is mapped to the NVRAM page whom address is
			* given as parameter.
			*
			* @param addr The addr to check.
			* @return A boolean which indicates if the DRAM is mapped to addr.
			*/
			inline bool mappedTo(unsigned long long addr){
				if(mapped())
					if(pageMapped_->getBeginAddr() == addr)
						return true;
				return false;
			}

			/**
			* Getter for dirty state.
			*
			* @return The value of the dirty_ class member.
			*/
			inline bool dirty(){
				return dirty_;
			}

			/**
			* Setter for the dirty state.
			*
			* @param dirty The class member dirty_ will take this value.
			*/
			inline void setDirty(bool dirty){
				dirty_ = dirty;
			}

		private :
			clock_t lastAccess_; /**< The last access date of this DRAM
			page. */
			bool dirty_; /**< Boolean which indicates if the page is dirty (i.e.
			modified) or not. */

			NVRAMPage *pageMapped_; /**< The NVRAMPage which is mapped to this
			DRAMPage. */
	};
}

#endif