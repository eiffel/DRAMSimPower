/*
* Copyright (c) 2017, Francis Laniel, francis.laniel@lip6.fr
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "PageTable.h"
#include <iostream>
#include <limits.h>

using std::vector;
using std::cout;
using std::hex;
using std::dec;
using std::endl;
using std::cerr;
using Caches::Page;
using Caches::PageTable;

/**
* PageTable constructor.
* It does nothing.
*/
PageTable::PageTable(){
}

/**
* PageTable destructor.
* It just deletes the DRAMPage allocated in constructor.
*/
PageTable::~PageTable(){
	vector<VirtualPage *>::iterator it;

	for(it = pageTable_.begin(); it != pageTable_.end(); ++it){
		/*
		* BUG This line will be problematic because we do not difference NVRAMPage
		* and DRAMPage (the second are deleted in CachLru destructor).
		* TODO Use the same dirty trick than used in physicalAddressAlreadyUsed()
		* to well delete object.
		*/
		delete (*it)->getPhysicalPage();

		delete *it;
	}
}

/**
* Test if a VirtualPage is contained in LRU.
*
* @param vPage The VirtualPage to search.
* @return If vPage produced an HIT it returns the PhysicalPage to which vPage is
* mapped, it returns NULL otherwise.
*/
Page *PageTable::testMapped(VirtualPage *vPage){
	Page *page;
	vector<VirtualPage *>::const_iterator it;

	for(it = pageTable_.begin(); it != pageTable_.end(); ++it)
		if((*it)->getBeginAddr() == vPage->getBeginAddr()){
			page = (*it)->getPhysicalPage();

			if(not page){
				cerr << RED << "This case should not happen since the VirtualPage is "
				"present into PageTable it must have a PhysicalPage." <<
				NO_COLOR << endl;

				exit(EXIT_FAILURE);
			}

			return page;
		}
	return NULL;
}

/**
* This function handle a page fault.
* It will search a free PhysicalPage and map the VirtualPage to it.
*
* @param vPage the VirtualPage which will be mapped.
* @return the (Physical)Page to which the VirtualPage was mapped.
*/
Page *PageTable::doPageFault(VirtualPage *vPage){
	Page *toMap;

	unsigned long long addr;

	cout << "Transaction to map a NVRAM to a DRAM are not implemented." << endl;

	do{
		addr = PageTable::generateRandomPageAddr();
	}while(physicalAddressAlreadyUsed(addr));

	toMap = new Page(addr);

	vPage->setPhysicalPage(toMap);

	// Insert the newly mapped VirtualPage in pageTable_.
	pageTable_.push_back(vPage);

	cout << "Virtual page is now mapped to physical page whom beginning" <<
	" address is '0x" << hex << addr << dec << "'." << endl;

	return toMap;
}

/**
* This function makes a complete transaction in the page table.
*
* If the VirtualPage given in parameter is not present it will trigger a page
* fault and will be mapped to a (Physical)Page.
*
* @param vPage The VirtualPage to search in the page table.
* @return true if the transaction made an HIT or false otherwise.
*/
bool PageTable::transaction(VirtualPage *vPage){
	bool ret;

	Page *page;
	PhysicalPage *pPage;

	if(not vPage){
		cerr << RED << "Argument is NULL." << NO_COLOR << endl;

		exit(EXIT_FAILURE);
	}

	ret = true;

	page = testMapped(vPage);

	if(not page){
		cout << "Page fault on page whom beginning address is '0x" << hex <<
		vPage->getBeginAddr() << dec << "'." << endl;

		ret = false;

		page = doPageFault(vPage);
	}else{
		cout << "HIT on page whom beginning address is '0x" << hex <<
		vPage->getBeginAddr() << dec << "'." << endl;
	}

	// We only dynamic_cast at the end of the if to avoid copying/pasting of code.
	pPage = dynamic_cast<PhysicalPage *>(page);

	if(pPage == NULL){
		cerr << RED << "dynamic_cast to DRAMPage* failed." <<
		NO_COLOR << endl;

		exit(EXIT_FAILURE);
	}

	return ret;
}

/**
* This function generates a random beginning page address.
*
* @return A random generated beginning page address.
*/
unsigned long long PageTable::generateRandomPageAddr(){
	size_t i;
	size_t ratio;

	unsigned long long ret;

	ratio = sizeof ret / sizeof(int);

	ret = 0x0;

	/*
	* 'rand()' return an int but our address will be larger than int.
	* We need to call multiple time 'rand()' to have the good number of random
	* bit.
	* For example, if we have sizeof(ret) = 8, sizeof(int) = 4 and CHAR_BIT = 8
	* we will iterate 2 times :
	* 1. ret |= rand() << 0 * (4 * 8); // For example ret is now 0x1234 5678
	* 2. ret |= rand() << 1 * (4 * 8); // Now ret is 0xFFFF ABCD 1234 5678
	*
	* NOTE Each iteration after the first one is randomly done, so we can have
	* small address.
	*
	* We need to cast the 'rand()' to unsigned long long to have the good bit
	* length.
	*/
	for(i = 0; i < ratio; i++)
		if(rand() % 2 && i > 0)
			continue;
		else
			ret |= ((unsigned long long) rand()) << i * (sizeof(int) * CHAR_BIT);

	/*
	* TODO Actually this generator generates a sizeof(long long) * CHAR_BIT
	* address. But it is possible to generate an address which has not reality
	* because there is not enough memory.
	* We need to modulo this by the size of NVRAM.
	*/
	return Page::align(ret);
}

/**
*
* @return true if the physical address given as parameter is already used by
* a PhysicalPage
*/
bool PageTable::physicalAddressAlreadyUsed(unsigned long long pAddr){
	DRAMPage *dPage;
	unsigned long long addr;

	vector<VirtualPage *>::const_iterator it;

	for(it = pageTable_.begin(); it != pageTable_.end(); ++it){
		dPage = dynamic_cast<DRAMPage *>((*it)->getPhysicalPage());

		/*
		* ALERT This 'if' is a lack of genericity since we specify the PhysicalPage.
		* In the LRU strategy a VirtualPage can be mapped to a NVRAMPage or to a
		* DRAMPage. This function is used to test if pAddr is not used as a
		* beginning address of a NVRAMPage.
		* If the VirtualPage is mapped to a DRAMPage (due to the LRU) we need to
		* get the NVRAMPage mapped to this DRAMPage.
		* WARNING Actually the VirtualPage are not mapped to DRAMPage so this
		* snippet is never used. But it will need to be tested.
		*/
		if(dPage)
			try {
				addr = dPage->getNVAddr();
			}catch(char *e){
				cerr << RED << e << NO_COLOR << endl;

				exit(EXIT_FAILURE);
			}
		else
			/*
			* Since the pageTable_ vector must only contains mapped VirtualPage we do
			* not need to check if 'getPhysicalPage()' differs from NULL.
			* Otherwise there is a big bug in the code I wrote and if it explodes here
			* it will be good to explode.
			*/
			addr = (*it)->getPhysicalPage()->getBeginAddr();

		if(addr == pAddr)
			return true;
	}

	return false;
}