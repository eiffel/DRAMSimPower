/*
* Copyright (c) 2017, Francis Laniel, francis.laniel@lip6.fr
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef PAGE_H
#define PAGE_H

#include <cstdlib>

// Used to print in color.
#ifdef __unix__
#define RED "\033[0;31m"
#define NO_COLOR "\033[0m"
#endif

#ifdef _WIN32
#define RED ""
#define NO_COLOR ""
#endif

namespace Caches{
	/**
	* Our cache strategies will manipulate page.
	*
	* Each page has a beginning address and a size, so the addresses which are
	* members of the page are included in [beginAddr_; beginAddr_ + size].
	* Each page has also a dirty flag to inform if the page was modified or not,
	* this flag will be possibly used in the cache strategies.
	*/
	class Page {
		public :
			Page(unsigned long long addr);
			/*
			* 'virtual' keyword is just here to make the class polymorphic so it can
			* be dynamic casted to girls classes.
			*/
			virtual ~Page();

			/**
			* Getter for the page size.
			*
			* @return The value of the size static variable.
			*/
			static inline size_t getPageSize() {
				return Page::size;
			}

			/**
			* Setter for the page size.
			*
			* @param size The value of the page's size.
			*/
			static inline void setPageSize(size_t size) {
				Page::size = size;
			}

			/**
			* Align an address on Page::size.
			*
			* @param addr The address which will be aligned.
			* @return The address aligned on Page::size.
			*/
			static inline unsigned long long align(unsigned long long addr){
				/*
				* Since Page::size is a size_t we need to cast it to unsigned long long
				* so the bitwise will be on the same bit length.
				* We need to sub 1 from Page::size because a Page contains bytes from
				* beginning address and beginning address + Page::size - 1.
				* For example if we have Page whom size is 4096 bytes then the bytes 0
				* to 4095 will be in the first page and the byte 4096 will be in the
				* second page.
				*/
				return addr & ~((unsigned long long) Page::size - 1);
			}

			/**
			* test if an address is aligned on Page::size
			*
			* @param addr The address to test.
			* @return true if the address is aligned, false otherwise.
			*/
			static inline bool aligned(unsigned long long addr){
				/*
				* We '&' the addr with Page::size - 1 to check if one of the
				* log2(Page::size) least significant bit of addr is '1'.
				* If it is the case the result of '&' will be positive or 0 otherwise.
				* We then 'not' the result to have a good boolean.
				*/
				return not (addr & ((unsigned long long) Page::size - 1));
			}

			/**
			* Getter for the beginning address of the page.
			*
			* @return The value of the beginAddr_ class member.
			*/
			inline unsigned long long getBeginAddr() {
				return beginAddr_;
			}

		private :
			static size_t size; /**< The pages' size in byte. To simplify the model
			there will only be one page size for all pages. */

			unsigned long long beginAddr_; /**< The beginning address of the page. */
	};

	/**
	* A PhysicalPage will have the same fields than a classical Page.
	* This typedef is just here for a better understanding.
	*/
	typedef Page PhysicalPage;

	/**
	* A NVRAMPage will have the same fields than a classical Page.
	* This typedef is just here for a better understanding.
	*/
	typedef PhysicalPage NVRAMPage;
}
#endif