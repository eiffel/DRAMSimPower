#include <iostream>
#include <cstdlib>
#include <cstring>
#include "Timings.h"
#include "Page.h"

using std::vector;
using std::cerr;
using std::endl;
using std::free;
using DRAMSim::Timings;

/**
* Timings constructor.
* It does nothing
*/
Timings::Timings(){
}

/**
* Timings destructor.
* TODO Release the memory which was malloced for timings' 'name'.
*/
Timings::~Timings(){
	vector<Timing>::iterator it;

	for(it = timings_.begin(); it != timings_.end(); ++it)
		free(it->name);
}

/**
* Getter for the value of the timing which name is given as a parameter.
* If there is no timing associated to 'name' the function will exit the entire
* program.
*
* @param name The name of the timing to search.
* @return The value associated to the timing 'name'.
*/
unsigned Timings::getTiming(const char *name){
	vector<Timing>::const_iterator it;

	for(it = timings_.begin(); it != timings_.end(); ++it)
		if(not strcmp(it->name, name))
			return it->value;

	cerr << RED "There is no existing timing for key '" << name << "'" NO_COLOR
	<< endl;

	exit(EXIT_FAILURE);
}

/**
* Set a new pair {name, value} into the timings_ vector.
* If there is already a timing named 'name' its value is updated.
*
* @param name The name of the timing to add.
* @param value Its value.
*/
void Timings::setTiming(char *name, unsigned value){
	Timing toAdd;
	vector<Timing>::iterator it;

	for(it = timings_.begin(); it != timings_.end(); ++it)
		if(not strcmp(it->name, name)){
			it->value = value;

			return;
		}

	toAdd = {strdup(name), value};

	// 'name' was nos already present into the vector, add it at the end.
	timings_.push_back(toAdd);
}