/*
* Copyright (c) 2017, Francis Laniel, francis.laniel@lip6.fr
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Cache.h"
#include "CacheLru.h"
#include <iostream>
#include <algorithm>


namespace Caches {
	using std::transform;
	using std::string;

	/**
	* Convert a string to an enum CacheStrategy.
	*
	* @param str The string which will be convert.
	* @return The CacheStrategy which coincides or CacheStrategy::LRU.
	*/
	enum CacheStrategy fromString(string str){
		transform(str.begin(), str.end(), str.begin(), ::toupper);

		if(not str.compare("LRU"))
			return CacheStrategy::LRU;

		return CacheStrategy::LRU;
	}
}

using std::cerr;
using std::cout;
using std::endl;
using Caches::Cache;

/**
* Cache factory.
*
* @param strategy The strategy of the cache which have to be created.
* @param bytes The size of the cache.
* @return A pointer on the cache just created, the return value must be
* specialized through dynamic_cast and will have to be deleted.
*/
Cache *Cache::cacheFactory(enum CacheStrategy strategy,
													 unsigned long long bytes){
	switch(strategy){
		case LRU :
			return new CacheLru(bytes);
		default :
			cerr << RED << "Cache strategy '" << strategy << "' is unknown." <<
			NO_COLOR << endl;
			exit(EXIT_FAILURE);
	}
}

/**
* Cache destructor.
*
* It does nothing.
*/
Cache::~Cache(){
}