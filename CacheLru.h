/*
* Copyright (c) 2017, Francis Laniel, francis.laniel@lip6.fr
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef CACHELRU_H
#define CACHELRU_H

#include <cstdlib>
#include <vector>
#include <map>

#include "Cache.h"
#include "DRAMPage.h"

namespace Caches{
	class CacheLru : public Cache{
		public :
			CacheLru(unsigned long long bytes = Page::getPageSize());
			~CacheLru();

			Page *testHit(NVRAMPage *nvPage);
			Page *doMiss(NVRAMPage *nvPage);
			Page *oust();
			bool transaction(NVRAMPage *nvPage);

			void updateLastAccess();

			/**
			* Test if the LRU is full.
			*
			* @return true if the LRU is full, false otherwise.
			*/
			inline bool full(){
				return page_nr_ == mapped_nr_;
			}

			/**
			* Getter for the number of page contained in LRU.
			*
			* @return The value of the page_nr_ class member.
			*/
			inline unsigned long long getPageNumber(){
				return page_nr_;
			}

			/**
			* Getter for the number of NVRAM page which are actually mapped in LRU.
			*
			* @return The value of mapped_nr_ class member.
			*/
			inline unsigned long long getMappedPageNumber(){
				return mapped_nr_;
			}

		private :
			unsigned long long page_nr_; /**< Number of DRAMPage of the LRU. It is
			the size of the pages_ vector. */
			std::vector<DRAMPage *> pages_; /**< The LRU itself. It is a vector of
			pointers on DRAMPage. */

			unsigned long long mapped_nr_; /**< Number of NVRAM pages which are mapped
			in the LRU. It is the size of the directory_ map. */
			std::map<NVRAMPage *, DRAMPage *> directory_; /**< This member will be
			used in testHit function. The key of this map will be a NVRAMPage pointer
			and the value DRAMPage pointer where it is mapped.
			To search if a NVRAMPage is already mapped we just need to search if
			there is an entry with the key which is equal to the begining address of
			considered NVRAMPage.
			If it is the case we have an HIT and we can get the DRAMPage associated to
			it. However it is a MISS and we need to call the doMiss function. */
	};
}

#endif