#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <bitset>
#include <string.h>
#include <assert.h>
#include "../Page.h"
#include "../VirtualPage.h"
#include "../PageTable.h"
#include "../IniReader.h"
#include "../SystemConfiguration.h"
#include "../Timings.h"

using std::string;
using std::map;
using std::ifstream;
using std::stringstream;
using std::cout;
using std::endl;
using std::hex;
using std::dec;
using std::bitset;
using Caches::Page;
using Caches::PhysicalPage;
using Caches::VirtualPage;
using Caches::PageTable;
using namespace DRAMSim;

/**
* This function checks that the bits used to guess the rank are not in the bits
* used for page offsetting.
* If it is the case the function will exit the program.
*
* @return The shift value to get the first bit used to guess the rank.
*/
unsigned checkMapping(void){
	unsigned bitsToRank;
	unsigned bitsOffsetInPage;
	unsigned colHighBitWidth;

	/*
	* Obtain the number of bit which are used as offset in into a page.
	* For example, if page's size is 4096 bytes then there will be 12 bits used
	* as offset (2^12 = 4096).
	*/
	bitsOffsetInPage = dramsim_log2(Page::getPageSize());

	colHighBitWidth = NUM_COLS_LOG - COL_LOW_BIT_WIDTH;

	/*
	* The output of a rank is always JEDEC_DATA_BUS_BITS (which is 64 bits =
	* 8 bytes) so we add the bits which correspond to the offset of one output.
	* DDR permits to read/write in burst, so a transaction will activate multiple
	* column (2 for DDR, 4 for DDR2, 8 for DDR3 and 8 for DDR4). We also need to
	* add those bits.
	*/
	bitsToRank = BYTE_OFFSET_WIDTH + COL_LOW_BIT_WIDTH;

	/*
	* This switch tests which mapping scheme is used by the memory controller.
	* We will add the bits consequently.
	*/
	switch(addressMappingScheme){
		case Scheme1 : // chan:rank:row:col:bank
			bitsToRank += NUM_BANKS_LOG + colHighBitWidth + NUM_ROWS_LOG;

			break;
		case Scheme2 : // chan:row:col:bank:rank
			break;
		case Scheme3 : // chan:rank:bank:col:row
			bitsToRank += NUM_ROWS_LOG + colHighBitWidth + NUM_BANKS_LOG;

			break;
		case Scheme4 : // chan:rank:bank:row:col
			bitsToRank += colHighBitWidth + NUM_ROWS_LOG + NUM_BANKS_LOG;

			break;
		case Scheme5 : // chan:row:col:rank:bank
			bitsToRank += NUM_BANKS_LOG;

			break;
		case Scheme6 : // chan:row:bank:rank:col
			bitsToRank += colHighBitWidth;

			break;
		case Scheme7 : // row:col:rank:bank:chan
			bitsToRank += NUM_CHANS_LOG + NUM_BANKS_LOG;

			break;
		default :
			cerr << RED "The mapping scheme is unknown." NO_COLOR << endl;

			exit(EXIT_FAILURE);
	}

	if(bitsToRank < bitsOffsetInPage){
		cerr << RED "The bits used to guess the rank are hidden into the bits "
		"used for page offsetting (" << bitsToRank << " < " << bitsOffsetInPage <<
		")" NO_COLOR << endl;

		exit(EXIT_FAILURE);
	}

	return bitsToRank;
}

int main(int argc, char **argv){
	int ret;

	size_t omniscientLimit;
	size_t dramRankNr;

	unsigned long long megsOfMemory;
	unsigned long long megsOfStoragePerRank;

	unsigned shiftToRank;

	string line;
	char *str;
	char *stringAddr;

	unsigned long long addr;
	unsigned long long mask;

	map<VirtualPage *, unsigned long long> pages;
	map<VirtualPage *, unsigned long long>::iterator it;

	unsigned rank;

	Page *page;

	char *opType;
	time_t timeOp;
	unsigned long long alignAddr;
	unsigned long long physicalAddr;

	// This pointer will only be used to pleased the ReadIniFile signature...
	Timings *t;

	size_t dramAccess;
	size_t nvramAccess;

	if(argc < 6){
		fprintf(stderr, "Usage : %s valgrind_trace system.ini dram.ini"
		" dram_rank_number memory_size(Megabytes) omniscient_limit [page_size]\n", argv[0]);

		exit(EXIT_FAILURE);
	}

	ret = EXIT_SUCCESS;

	t = new Timings();

	// This function reads the system.ini file.
	IniReader::ReadIniFile(string(argv[2]), true, t);

	/*
	* And this one update the value of addressMappingScheme depending of what was
	* read in system.ini file.
	*/
	IniReader::InitEnumsFromStrings();

	IniReader::ReadIniFile(string(argv[3]), false, t);

	dramRankNr = strtoull(argv[4], NULL, 10);
	omniscientLimit = strtoull(argv[6], NULL, 10);

	megsOfMemory = strtoull(argv[5], NULL, 10);
	megsOfStoragePerRank = ((((unsigned long long) NUM_ROWS *
	(NUM_COLS * DEVICE_WIDTH) * NUM_BANKS) *
	((unsigned long long) JEDEC_DATA_BUS_BITS / DEVICE_WIDTH)) / 8) >> 20;

	NUM_RANKS = megsOfMemory / megsOfStoragePerRank;

	if(NUM_RANKS == 0){
		cerr << RED "The number of rank can not be 0."
		NO_COLOR << endl;

		exit(EXIT_FAILURE);
	}

	if(NUM_RANKS <= dramRankNr){
		cerr << RED "The number of DRAM rank is superior than total rank. Check "
		"the argv" NO_COLOR << endl;

		exit(EXIT_FAILURE);
	}

	NUM_RANKS_LOG = dramsim_log2(NUM_RANKS);

	if(argc == 8)
		Page::setPageSize(strtoull(argv[7], NULL, 10));

	shiftToRank = checkMapping();

	ifstream valgrindTrace(argv[1]);

	ofstream dramSimTrace(string(argv[1]) + ".trc");

	if(not valgrindTrace.is_open()){
		cerr << RED "Problem while trying to open '" << argv[1] << "'." NO_COLOR <<
		endl;

		exit(EXIT_FAILURE);
	}

	// valgrind_trace are CSV file. This call eliminates the header line.
	getline(valgrindTrace, line);

	while(getline(valgrindTrace, line)){
		/*
		* I do not want to dive into how to tokenize a string in C++, so I will do
		* it in C. This little handiwork permits to convert a std::string to a
		* classical C char *.
		*/
		str = &line[0];

		// This call eliminates the type of access.
		strtok(str, ";");

		stringAddr = strtok(NULL, ";");

		if(!stringAddr){
			cerr << RED "The CSV is broken !" NO_COLOR << endl;

			ret = EXIT_FAILURE;

			goto release;
		}

		// 16 because the address are under hexa form.
		addr = Page::align(strtoull(stringAddr, NULL, 16));

		// Search in the pages map if the address of the page was already accessed.
		for(it = pages.begin(); it != pages.end(); ++it){
			if(it->first->getBeginAddr() == addr){
				/*
				* It is the case, so increment the number of time this page was
				* accessed and exit the loop.
				*/
				it->second++;

				break;
			}
		}

		if(it == pages.end())
			/*
			* The page was not in the pages map.
			* So add a new VirtualPage and set its count access to 1.
			*/
			pages[new VirtualPage(addr)] = 1;
	}

	/*
	* This mask contains NUM_RANKS_LOG bits which are 1 and the others are 0.
	* To create it we take the 2^NUM_RANKS_LOG - 1 and we shift it.
	*/
	mask = (1ULL << NUM_RANKS_LOG) - 1;
	mask <<= shiftToRank;

	for(it = pages.begin(); it != pages.end(); ++it){
		/*
		* The address generated is a physical address then it can not be greater
		* than the total amout of memory we have.
		* We shift the megsOfMemory by 20 to get the size in bytes.
		*/
		addr = PageTable::generateRandomPageAddr() % (megsOfMemory << 20);

		/*
		* We need to clear the bits used to guess the rank. To explain we will take
		* two examples, we consider addresse 0b1111 0000 1010 0011 and shiftToRank
		* = 12
		* If we have 4 ranks then the mask is 0b0011 0000 0000 0000 so address will
		* be 0b1100 0000 1010 0011.
		* If we have 8 ranks then the mask is 0b0111 0000 0000 0000 and the adress
		* will be 0b1000 0000 1010 0011.
		*/
		addr &= ~mask;

		if(it->second > omniscientLimit)
			/*
			* The DRAM rank will be the first dramRankNr so we just need to random on
			* this value.
			*/
			rank = rand() % dramRankNr;
		else
			/*
			* NVRAM rank will be the last NUM_RANKS - dramRankNr ranks so we random on
			* this value and add dramRankNr.
			*/
			rank = rand() % (NUM_RANKS - dramRankNr) + dramRankNr;

		addr |= (unsigned long long) rank << shiftToRank;

		if(not Page::aligned(addr)){
			cerr << RED "0x" << hex << addr << dec << " not aligned on page's size."
			NO_COLOR << endl;

			ret = EXIT_FAILURE;

			goto release;
		}

		it->first->setPhysicalPage(new PhysicalPage(addr));
	}

	/*
	* Instead of reopening a new file we prefer to seek the already open at the
	* begining of its offset.
	*/
	valgrindTrace.clear();
	valgrindTrace.seekg(0, valgrindTrace.beg);

	if(!dramSimTrace.is_open()){
		cerr << "Error while trying to open '" << argv[1] << ".trc'" << endl;

		ret = EXIT_FAILURE;

		goto release;
	}

	getline(valgrindTrace, line);

	timeOp = 0;

	dramAccess = 0;
	nvramAccess = 0;

	while(getline(valgrindTrace, line)){
		str = &line[0];

		opType = strtok(str, ";");

		if(strcmp(opType, "IFECTH") == 0)
			opType = "IFETCH";

		addr = strtoull(strtok(NULL, ";"), NULL, 16);

		alignAddr = Page::align(addr);

		for(it = pages.begin(); it != pages.end(); ++it){
			if(it->first->getBeginAddr() == alignAddr){
				physicalAddr = it->first->getPhysicalPage()->getBeginAddr();

				/*
				* Get the offset bits of the address. For example if we consider page of
				* 4096 bytes and the address 0x1001 :
				* addr : 			0b0001 0000 0000 0001
				* 4096 - 1 :	0b0000 1111 1111 1111
				* result :		0b0000 0000 0000 0001
				*
				* TODO This operation can be useful so it can be good to make a function
				* for example Page::getOffset().
				*/
				addr &= (Page::getPageSize() - 1);

				/*
				* Add the offset of the virtual address to the physical address.
				* We are making the assumption that the handling of virtual to physical
				* page translation is the same than in Linux where the offset bits are
				* the same.
				*/
				physicalAddr |= addr;

				// Get the rank of the physical address.
				rank = (physicalAddr & mask) >> shiftToRank;

				/*
				* According to the rank we increment the good access variable.
				* Those variables are used to check if DRAMSim make the same access.
				*/
				if(rank < dramRankNr)
					dramAccess++;
				else
					nvramAccess++;

				break;
			}
		}

		if(it == pages.end()){
			cerr << RED "The page must be present in the pages map." NO_COLOR << endl;

			ret = EXIT_FAILURE;

			goto release;
		}

		dramSimTrace << "0x" << hex << physicalAddr << dec << " " << opType << " "
		<< timeOp << endl;

		timeOp += strtoull(strtok(NULL, ";"), NULL, 10);
	}

	cout << "#Access to DRAM : '" << dramAccess << "'" << endl;
	cout << "#Access to NVRAM : '" << nvramAccess << "'" << endl;

release :
	delete t;

	for(it = pages.begin(); it != pages.end(); ++it){
		page = it->first->getPhysicalPage();

		if(page)
			delete page;

		delete it->first;
	}

	valgrindTrace.close();

	ifstream test(string(argv[1])+ ".trc", ios::ate | ios::binary);

	if(not test.tellg())
		remove(strcat(argv[1], ".trc"));

	test.close();

	/*
	* Due to a compiler error which wants to enclose goto skipped section
	* with brackets I decided to put the close of the output file here.
	* But it is possible that this file was not written. It would be sad to
	* create an empty file for nothing.
	* So I tested its size and removed it if it is 0.
	*/
	dramSimTrace.close();


	return ret;
}