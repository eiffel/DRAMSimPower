/*
* Copyright (c) 2017, Francis Laniel, francis.laniel@lip6.fr
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <assert.h>
#include "TestPageTable.h"
#include "../PageTable.h"
#include "../Page.h"
#include "../VirtualPage.h"

using std::vector;
using std::cout;
using std::endl;
using Caches::Page;
using Caches::VirtualPage;
using Caches::PageTable;
using Caches::TestPageTable;

/**
* TestPageTable constructor.
* It does nothing.
*/
TestPageTable::TestPageTable(){
}

/**
* TestPageTable destructor.
* It does nothing.
*/
TestPageTable::~TestPageTable(){
}

/**
* The goal of this function is to run unit test on PageTable methods.
*
* @return EXIT_SUCCESS all the time. If a test does not pass the programm will
* be aborted.
*/
int TestPageTable::test(void){
	size_t i;

	vector<VirtualPage *> vPages;

	PageTable *pt;
	Page *p;

	Page::setPageSize(PAGE_SIZE);

	pt = new PageTable();

	for(i = 0; i < ITER; ++i){
		vPages.push_back(new VirtualPage(i * PAGE_SIZE));

		assert(Page::aligned(vPages[i]->getBeginAddr()));

		pt->transaction(vPages[i]);

		p = pt->testMapped(vPages[i]);

		assert(p);
		assert(Page::aligned(p->getBeginAddr()));

		assert(pt->physicalAddressAlreadyUsed(vPages[i]->getPhysicalPage()->
		getBeginAddr()));

		assert(PageTable::generateRandomPageAddr() >= (1 << 32));
	}

	delete pt;

	return EXIT_SUCCESS;
}