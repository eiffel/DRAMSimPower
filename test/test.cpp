/*
* Copyright (c) 2017, Francis Laniel, francis.laniel@lip6.fr
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <string>
#include "../Page.h"
#include "Test.h"
#include "TestLru.h"
#include "TestPageTable.h"
#include "TestTimings.h"

using std::string;
using std::cout;
using std::endl;
using std::cerr;
using Caches::Test;
using Caches::TestLru;
using Caches::TestPageTable;
using Caches::TestTimings;

int main(void){
	Test *test;
	string str;

#ifdef TEST_LRU_MAIN
	test = new TestLru();

	str = "Lru";
#elif defined TEST_TIMINGS_MAIN
	test = new TestTimings();

	str = "Timings";
#elif
	test = new TestPageTable();

	str = "Page Table";
#endif

	if(test->test() == EXIT_SUCCESS)
		cout << GREEN "Tests '" << str << "' were passed succesfully." NO_COLOR <<
		endl;
	else
		cerr << RED "Tests '" << str << "'  encountered a problem." NO_COLOR <<
		endl;

	delete test;

	return EXIT_SUCCESS;
}