/*
* Copyright (c) 2017, Francis Laniel, francis.laniel@lip6.fr
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <assert.h>
#include "TestTimings.h"
#include "../Timings.h"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using DRAMSim::Timings;
using Caches::TestTimings;

/**
* TestTimings constructor.
* It does nothing.
*/
TestTimings::TestTimings(){
}

/**
* TestTimings destructor.
* It does nothing.
*/
TestTimings::~TestTimings(){
}

/**
* The goal of this function is to run unit test on CacheLru methods.
*
* @return EXIT_SUCCESS if all the test pass. If a test does not pass the program
* will be aborted.
*/
int TestTimings::test(void){
	Timings timings;

	timings.setCK(12);
	assert(timings.getCK() == 12);

	timings.setTiming(to_str(timings), 120);
	assert(timings.getTiming(to_str(timings)) == 120);

	timings.setTiming("pear", 1200);
	assert(timings.getTiming("pear") == 1200);

	timings.setTiming("pear", 120);
	assert(timings.getTiming("pear") == 120);

	return EXIT_SUCCESS;
}