/*
* Copyright (c) 2017, Francis Laniel, francis.laniel@lip6.fr
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <assert.h>
#include "TestLru.h"
#include "../CacheLru.h"
#include "../Page.h"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using Caches::Cache;
using Caches::CacheLru;
using Caches::Page;
using Caches::DRAMPage;
using Caches::NVRAMPage;
using Caches::TestLru;

/**
* TestLru constructor.
* It does nothing.
*/
TestLru::TestLru(){
}

/**
* TestLru destructor.
* It does nothing.
*/
TestLru::~TestLru(){
}

/**
* The goal of this function is to run unit test on CacheLru methods.
*
* @return EXIT_SUCCESS if all the test pass or EXIT_FAILURE if a particular test
* does not pass. If a test does not pass the program will be aborted.
*/
int TestLru::test(void){
	int ret;
	size_t i;

	vector<NVRAMPage *> nvPages;

	Cache *lru;
	DRAMPage *casted;

	ret = EXIT_SUCCESS;

	Page::setPageSize(PAGE_SIZE);

	lru = Cache::cacheFactory(Caches::fromString("lru"), PAGE_NR * PAGE_SIZE);

	if(not lru){
		cerr << RED << "dynamic_cast failed." << NO_COLOR << endl;

		exit(EXIT_FAILURE);
	}

	for(i = 0; i < ITER; ++i){
		nvPages.push_back(new NVRAMPage(i * PAGE_SIZE));

		assert(Page::aligned(nvPages[i]->getBeginAddr()));

		if(i < PAGE_NR){
			assert(not lru->transaction(nvPages[i])); // should produce a MISS.

			assert(lru->testHit(nvPages[i]));
		}
	}

	assert(lru->full()); // After this loop the lru should be full.

	assert(lru->transaction(nvPages[0])); // This request should produce a HIT.

	casted = dynamic_cast<DRAMPage *>(lru->oust());

	if(not casted){
		cerr << RED << "dynamic_cast to DRAMPage* failed." <<
		NO_COLOR << endl;

		ret = EXIT_FAILURE;

		goto free;
	}

	/*
	* The previous transaction updated the last access date of addresses[0].
	* So the least recently used should be addresses[1].
	*/
	assert(casted->getNVAddr() == nvPages[1]->getBeginAddr());

free:
	delete lru; // The 'new' was done in cacheFactory, we need to delete it.

	for(i = 0; i < ITER; ++i)
		delete nvPages[i];

	return ret;
}